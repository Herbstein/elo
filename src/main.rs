#[derive(Debug, Clone, Copy)]
pub struct EloRating {
    rating: f64,
}

impl EloRating {
    pub fn new(rating: f64) -> Self {
        Self { rating }
    }

    pub fn calcualate_q(&self) -> f64 {
        10f64.powf(self.rating / 2000.0)
    }
}

#[derive(Clone, Copy)]
pub struct EloSystem {
    k: f64,
}

impl EloSystem {
    pub fn new(k: f64) -> Self {
        Self { k }
    }

    pub fn find_frag_limit(&self, ratings: &[EloRating], frag_limit: u32) -> Vec<u32> {
        let qs = ratings.iter().map(|r| r.calcualate_q());
        let q_sum = qs.clone().sum::<f64>();

        let es = qs.map(|q| q / q_sum);
        let e_max = es.clone().max_by(|x, y| x.partial_cmp(y).unwrap()).unwrap();
        es.map(|e| (frag_limit as f64 * (e / e_max)) as u32)
            .collect()
    }

    pub fn update_ratings(&self, game_scores: &[(EloRating, u32)]) -> Vec<EloRating> {
        assert!(game_scores.len() >= 2);

        let max_elo_kills = game_scores
            .iter()
            .max_by(|(r1, _), (r2, _)| r1.rating.partial_cmp(&r2.rating).unwrap())
            .map(|(_, s)| *s)
            .unwrap();

        println!("Kills of top ELO player: {}\n", max_elo_kills);

        let frag_limits = self.find_frag_limit(
            &game_scores.iter().map(|(x, _)| *x).collect::<Vec<_>>(),
            max_elo_kills,
        );

        println!("Frag limits:");
        for (i, limit) in frag_limits.iter().enumerate() {
            println!("\tPlayer {} has {}/{} frag limit", i, limit, max_elo_kills);
        }

        let game_scores = game_scores
            .iter()
            .zip(frag_limits)
            .map(|((r, a), p)| (r, *a as f64 / p as f64))
            .collect::<Vec<_>>();

        println!("Game result:");
        for (i, (rating, score)) in game_scores.iter().enumerate() {
            println!(
                "\tPlayer {} has {} Elo and got {}% of their frag limit",
                i,
                rating.rating,
                score * 100.0
            );
        }

        let n = game_scores.len();
        let e_size = ((n - 1) * n) / 2;

        let mut matches = Vec::with_capacity(e_size);
        for i in 0..game_scores.len() {
            for j in 0..game_scores.len() {
                if i == j || matches.contains(&(j, i)) {
                    continue;
                }

                matches.push((i, j));
            }
        }

        println!("Virtual matches:");
        for (i, j) in matches.iter() {
            println!("\t{} vs {}", i, j);
        }

        let mut scores = vec![0.0; n];
        for (i, j) in matches {
            let i_score = game_scores[i].1;
            let j_score = game_scores[j].1;

            if i_score > j_score {
                scores[i] += 1.0;
            } else if i_score < j_score {
                scores[j] += 1.0;
            } else {
                scores[i] += 0.5;
                scores[j] += 0.5;
            }
        }

        println!("Scores:");
        for (i, score) in scores.iter().enumerate() {
            println!("\tPlayer {} has score {}", i, score);
        }

        let mut new_ratings = Vec::with_capacity(n);
        for (i, score) in scores.iter().enumerate() {
            let old_rating = game_scores[i].0.rating;
            let expected = 0.5 * (n - 1) as f64;
            let new_rating = old_rating + self.k * (score - expected);
            new_ratings.push(EloRating::new(new_rating));
        }

        println!("Updated ratings");
        for (i, rating) in new_ratings.iter().enumerate() {
            println!(
                "\tPlayer {} updates from {} to {}. Delta: {}",
                i,
                game_scores[i].0.rating,
                rating.rating,
                game_scores[i].0.rating - rating.rating,
            );
        }

        new_ratings
    }
}

fn main() {
    let system = EloSystem::new(24.0);

    let _ = system.update_ratings(&[
        (EloRating::new(1700.0), 12),
        (EloRating::new(2000.0), 8),
        (EloRating::new(1700.0), 20),
    ]);
}
